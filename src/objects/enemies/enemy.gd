extends CharacterBody2D


@export var active : bool = false
@export var speed : int = 1500
#@export var gravity : int = 1000
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
@export var direction : int = 1
@export var can_fall : bool = false
@export var sprite_flip : bool = false
@export var updown : int = 0
@export var health : int = 1
@export var wall_check_updown : int = 0
@export var death_res : PackedScene = preload("res://objects/enemies/mushroom_death.tscn")
var updown_wait = 0
#print(death_res)
#turn_delay prevents rapid turning back and forth
var turn_delay_start = 50
var turn_delay = turn_delay_start

@onready var death_obj = death_res.instantiate()
#var explosion = load("res://objects/explosion/explosion_2.tscn")


@onready var sprite = $sprite
@onready var ray1 = $RayCast2D
@onready var ray2 = $RayCast2D2
@onready var ground_check_r = $ground_check_r
@onready var ground_check_l = $ground_check_l
@onready var offscreen_timer = $offscreen_timer
@onready var ceiling_check_l = $ceiling_check_l
@onready var ceiling_check_r = $ceiling_check_r

var mover = Vector2.ZERO

func _ready():
	randomize()
	ray1.target_position.x *= direction
	ray2.target_position.x *= direction
	
func  _physics_process(delta):
	velocity.x = 0
	if turn_delay > 0:
		turn_delay-=1
		
	if active:
		walk(delta)
		move_updown(delta)
		velocity.y += gravity * delta
		move_and_slide()
		
		
func turn():
	direction *= -1
	ray1.target_position.x *= -1
	ray2.target_position.x *= -1

func move_updown(delta):
	if updown == 0:
		return
	
	updown_wait -= 1
	if updown_wait < 0:
		updown_wait = 100
		if ground_check_l.is_colliding() || ground_check_r.is_colliding():
			updown *= -1
		if ceiling_check_l.is_colliding() || ceiling_check_r.is_colliding():
			updown *= -1
		
	velocity.y += updown * delta
	
func set_direction():
	if direction == 1:
		if sprite_flip:
			sprite.flip_h = true
		else:
			sprite.flip_h = false
	else:
		if sprite_flip:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	
	sprite.play("walk") 
	
func wall_check(offset):
	ray1.target_position.y = offset
	if ray1.is_colliding() || ray2.is_colliding():
		if turn_delay < 1:
			turn_delay = turn_delay_start
			turn()
		
func walk(delta):
	
	
	if randi_range(0,3) > 2:
		wall_check(wall_check_updown)
	if randi_range(0,3) > 2:
		wall_check(-wall_check_updown)
	if randi_range(0,3) > 2:
		wall_check(0)
	
	if !can_fall:
		if !ground_check_l.is_colliding() || !ground_check_r.is_colliding():
			turn()
	
	
	set_direction()
		
	velocity.x += speed * delta * 2 * direction
	

func _on_offscreen_timer_timeout():
	#if gunner us off screen for 5 seconds then set to inactive
	active = false

func take_damage(amount):
	health -= 1
	if health < 1:
		death()

func death():
	get_tree().get_current_scene().add_child(death_obj)
	death_obj.position = position
	death_obj.position.y += 32
	queue_free()


func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group("explosion"):
		death()


func _on_visible_on_screen_notifier_2d_screen_entered():
	offscreen_timer.stop()
	active = true


func _on_visible_on_screen_notifier_2d_screen_exited():
	offscreen_timer.start()


func _on_attack_box_body_entered(body):
	if body.is_in_group("players"):
		body.death()
