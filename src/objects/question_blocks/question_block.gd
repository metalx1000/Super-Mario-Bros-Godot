extends StaticBody2D

var active = true
var player

@onready var spawner = preload("res://objects/coins/coin.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func hit():
	if active:
		active = false
		$AnimationPlayer.play("hit")
		spawn_item()
		
func spawn_item():
	var spawn = spawner.instantiate()
	spawn.position = position + Vector2(0,-64)
	get_tree().current_scene.add_child(spawn)
	spawn.death(player)

func death():
	$CollisionShape2D/AnimatedSprite2D.play("dead")
