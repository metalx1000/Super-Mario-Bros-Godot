extends Area2D

@export var value = 1
var active = true
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !active:
		position.y -= 50 * delta

func _on_body_entered(body):
	if !active:
		return
		
	if body.is_in_group("players"):
		$sprite.visible = false
		death(body)
		
func death(player):
	active = false
	var id = player.id
	player.coins += 1
	$sound.play()


func _on_sound_finished():
	queue_free()
