extends Node2D

var motion = 0
var speed = 800
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if motion == 0:
		position.y -= delta * speed
	elif motion == 1:
		pass
	elif motion > 1:
		position.y += delta * speed
	
	if motion == 6:
		get_tree().reload_current_scene()

func _on_timer_timeout():
	motion += 1
