extends CharacterBody2D

@export var id = 0
@export var SPEED = 400.0
@export var JUMP_VELOCITY = -1000.0
var score = 0
var state = "idle"
var dead = false
var coins = 0

@onready var dead_player =  preload("res://objects/player/dead_player.tscn")
@onready var animation = $AnimationPlayer
@onready var sprite = $Sprite2D
@onready var groundcast_l = $groundcast_l
@onready var groundcast_r = $groundcast_r
@onready var jump_snd = $jump_snd
@onready var collision = $CollisionShape2D
@onready var boxhit = $boxhit_1

var run_speed = 0
var run_power = 250
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = 2000

func _ready():
	pass

func _physics_process(delta):

	#die if fall
	if position.y > 1000:
		death()

	if dead:
		move_and_slide()
		return
		
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta
	#power button pressed
	if Input.is_action_pressed("power"):
		run_speed = run_power
	else:
		run_speed = 0
		
	# Handle Jump.
	if Input.is_action_just_pressed("player_jump") and is_on_floor():
		jump_snd.play()
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("player_left", "player_right")
	if direction:
		velocity.x = direction * (SPEED + run_speed) 
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED + run_speed)
	set_state()
	move_and_slide()
	
func attack(obj):
	if velocity.y< 1:
		return
		
	if obj.has_method("take_damage"):
		obj.take_damage(1)
		velocity.y = JUMP_VELOCITY / 2

func set_state():
	if velocity.x > 0:
		sprite.flip_h = false
	elif velocity.x < 0:
		sprite.flip_h = true

	if velocity.x != 0 && is_on_floor():
		state = "walk"
	elif !is_on_floor():
		state = "jump"
	else:
		state = "idle"
	
	animation.play(state)
	
func death():
	dead = true
	var dead_p = dead_player.instantiate()
	dead_p.position = position
	get_tree().current_scene.add_child(dead_p)
	queue_free()


func box_hit_check(body):
	if state != "jump" && velocity.y < 0:
		return
		
	
	if body.is_in_group("blocks"):
		body.player = self
		body.hit()
		
	if body is TileMap:
		var tilemap = body
		#tile is 1 higher than pososion to subtract 1 on y
		#print(to_global(collision.position))
		var tile = tilemap.local_to_map(to_global(collision.position)) -  Vector2i(0,1)
		#print(tile)
		var cell = tilemap.get_cell_atlas_coords(0,tile)
		
		if cell == Vector2i(24, 0):
			print("question mark")
			tilemap.erase_cell(0,tile)
		elif cell == Vector2i(1, 0):
			print("brick")
			tilemap.erase_cell(0,tile)
		else:
			print("unknown")

